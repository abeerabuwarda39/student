package com.example.student;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class studentAdapter extends RecyclerView.Adapter<studentAdapter.StudVh> {

    Context context ;
    List<student> studList;

    public studentAdapter(Context context ,  List<student> studList) {
        this.context = context;
        this.studList = studList;
    }

    @NonNull
    @Override
    public StudVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(context).inflate(R.layout.student , parent , false);
        return new StudVh(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudVh holder, int position) {
        holder.setData(studList.get(position));
    }

    @Override
    public int getItemCount() {
        return studList.size();
    }

    class StudVh extends RecyclerView.ViewHolder{
      TextView  student_name , student_id , student_level , student_avg;
        public StudVh(@NonNull View itemView) {
            super(itemView);
            student_name = itemView.findViewById(R.id.student_name);
            student_id = itemView.findViewById(R.id.student_id);
            student_level = itemView.findViewById(R.id.student_level);
            student_avg = itemView.findViewById(R.id.student_avg);
         }

        public void setData(student stud) {
            student_name.setText(stud.getName());
            student_id.setText(stud.getId());
            student_level.setText(stud.getLevel());
            student_avg.setText(stud.getAvg());
        }
    }
}
