package com.example.student;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.UserHandle;
import android.view.Display;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StudList extends Context {

     RecyclerView RV;
     studentAdapter studAdapter;
     ArrayList<student> stud = new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stud.add(new student("abeer" , "a123" , "3rd" , "90"));
        stud.add(new student("dana" , "ab4424" , "2nd" , "86"));
        stud.add(new student("ahmad" , "ba6234" , "5th" , "77"));
        stud.add(new student("wesam" , "b201" , "4th" , "95"));


        RV = RV.findViewById(R.id.RV_stud);
        RV.setLayoutManager(new LinearLayoutManager(this));
        studAdapter = new studentAdapter(this ,stud);
        RV.setAdapter(studAdapter);
    }

}

